package payunit

import (
	"context"
	"encoding/json"
	"net/http"
)

// GatewayService is the API client for the `/gateway` endpoint
type GatewayService service

// Makepayment makes a payment on Payunit
// NOTE: You must initialize a transaction using Initialize before calling Makepayment
// POST /gateway/makepayment
// API Doc: https://payunit.net/docs/rest-api/
func (service *GatewayService) Makepayment(ctx context.Context, payload GatewayMakepaymentRequestPayload) (*GatewayMakepaymentResponsePayload, *Response, error) {
	request, err := service.client.newRequest(ctx, http.MethodPost, "gateway/makepayment", payload)
	if err != nil {
		return nil, nil, err
	}

	resp, err := service.client.do(request)
	if err != nil {
		return nil, resp, err
	}

	var responsePayload GatewayMakepaymentResponsePayload
	if err = json.Unmarshal(*resp.Body, &responsePayload); err != nil {
		return nil, resp, err
	}

	return &responsePayload, resp, nil
}

// Initialize a transaction on PayUnit
// POST /gateway/initialize
// API Doc: https://payunit.net/docs/rest-api/
func (service *GatewayService) Initialize(ctx context.Context, payload GatewayInitializeRequestPayload) (*GatewayInitializeResponsePayload, *Response, error) {
	request, err := service.client.newRequest(ctx, http.MethodPost, "gateway/initialize", payload)
	if err != nil {
		return nil, nil, err
	}

	resp, err := service.client.do(request)
	if err != nil {
		return nil, resp, err
	}

	var responsePayload GatewayInitializeResponsePayload
	if err = json.Unmarshal(*resp.Body, &responsePayload); err != nil {
		return nil, resp, err
	}

	return &responsePayload, resp, nil
}
