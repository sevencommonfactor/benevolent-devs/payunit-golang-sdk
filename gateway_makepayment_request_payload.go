package payunit

type GatewayMakepaymentRequestPayload struct {
	Gateway       string  `json:"gateway"`
	Amount        uint    `json:"amount"`
	TransactionID string  `json:"transaction_id"`
	PhoneNumber   string  `json:"phone_number"`
	Currency      string  `json:"currency"`
	NotifyURL     *string `json:"notify_url"`
	Name          *string `json:"name"`
}
