package payunit

import (
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/url"
	"testing"
)

func TestWithHTTPClient(t *testing.T) {
	t.Run("httpClient is not set when the httpClient is nil", func(t *testing.T) {
		// Arrange
		config := defaultClientConfig()

		// Act
		WithHTTPClient(nil).apply(config)

		// Assert
		assert.NotNil(t, config.httpClient)
	})

	t.Run("httpClient is set when the httpClient is not nil", func(t *testing.T) {
		// Arrange
		config := defaultClientConfig()
		newClient := &http.Client{Timeout: 300}

		// Act
		WithHTTPClient(newClient).apply(config)

		// Assert
		assert.NotNil(t, config.httpClient)
		assert.Equal(t, newClient.Timeout, config.httpClient.Timeout)
	})
}

func TestWithBaseURL(t *testing.T) {
	t.Run("baseURL is not set when the url is nil", func(t *testing.T) {
		// Arrange
		config := defaultClientConfig()

		// Act
		WithBaseURL(nil).apply(config)

		// Assert
		assert.NotNil(t, config.baseURL)
	})

	t.Run("baseURL is set when the url is not nil", func(t *testing.T) {
		// Arrange
		newBaseURL, _ := url.Parse("https://example.com")
		config := defaultClientConfig()

		// Act
		WithBaseURL(newBaseURL).apply(config)

		// Assert
		assert.NotNil(t, config.baseURL)
		assert.Equal(t, newBaseURL.String(), config.baseURL.String())
	})
}

func TestWithAPIUser(t *testing.T) {
	t.Run("apiUser is set successfully", func(t *testing.T) {
		// Arrange
		config := defaultClientConfig()
		apiUser := "apiUser"

		// Act
		WithAPIUser(apiUser).apply(config)

		// Assert
		assert.Equal(t, apiUser, config.apiUser)
	})
}

func TestWithAPIPassword(t *testing.T) {
	t.Run("apiPassword is set successfully", func(t *testing.T) {
		// Arrange
		config := defaultClientConfig()
		apiPassword := "apiPassword"

		// Act
		WithAPIPassword(apiPassword).apply(config)

		// Assert
		assert.Equal(t, apiPassword, config.apiPassword)
	})
}

func TestWithAPIKey(t *testing.T) {
	t.Run("apiKey is set successfully", func(t *testing.T) {
		// Arrange
		config := defaultClientConfig()
		apiKey := "apiKey"

		// Act
		WithAPIKey(apiKey).apply(config)

		// Assert
		assert.Equal(t, apiKey, config.apiKey)
	})
}

func TestWithMode(t *testing.T) {
	t.Run("mode is set successfully", func(t *testing.T) {
		// Arrange
		config := defaultClientConfig()
		mode := "test"

		// Act
		WithMode(mode).apply(config)

		// Assert
		assert.Equal(t, mode, config.mode)
	})
}
