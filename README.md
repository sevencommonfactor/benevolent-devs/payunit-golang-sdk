# Payunit Golang SDK

[![Go Reference](https://pkg.go.dev/badge/gitlab.com/sevencommonfactor/benevolent-devs/payunit-golang-sdk.svg)](https://pkg.go.dev/gitlab.com/sevencommonfactor/benevolent-devs/payunit-golang-sdk)

This package provides a `go` client for interacting with the [Payunit REST API](https://payunit.net/docs/rest-api/)

## Installation

`payunit-go-sdk` is compatible with modern Go releases in module mode, with Go installed:

```bash
go get gitlab.com/sevencommonfactor/benevolent-devs/payunit-golang-sdk
```

Alternatively the same can be achieved if you use `import` in a package:

```go
import "gitlab.com/sevencommonfactor/benevolent-devs/payunit-golang-sdk"
```

## Implemented

- [Gateway](#gateway)
  - `POST /gateway/initialize` - initialize a transaction
  - `POST /gateway/makepayment` - make a payment
  
## Usage

### Initializing the Client

An instance of the `payunit` client can be created using `New()`.  The `http.Client` supplied will be used to make requests to the API.

```go
package main

import (
	"gitlab.com/sevencommonfactor/benevolent-devs/payunit-golang-sdk"
	"net/http"
)

func main()  {
	client := payunit.New(
		payunit.WithAPIKey("" /* Payunit API Key */),
		payunit.WithAPIUser("" /* Payunit API User */),
		payunit.WithAPIPassword("" /* Payunit API Password */),
		payunit.WithHTTPClient(http.DefaultClient),
	)
}
```


### Error handling

All API calls return an `error` as the last return object. All successful calls will return a `nil` error.

```go
payload, response, err := payunitClient.Gateway.Initialize(
	context.Background(),
	payunit.GatewayInitializeRequestPayload{}
)
if err != nil {
  //handle error
}
```

### Gateway

This handles all API requests whose URL begins with `/gateway/`

#### Initialize a transaction

`POST /gateway/initialize`: Initializes a transaction on PayUnit

```go
// Optional request parameters
var notifyUrl = "https://my.website.com/payunit/notify"
var appName = "your app name"
var purchaseRef = "your purchase reference"

response, _, err := payunitClient.Gateway.Initialize(
	context.Background(),
	payunit.GatewayInitializeRequestPayload{
		TotalAmount:   100, // Amount to be paid
		TransactionID: "unique transaction id", 
		Currency:      "XAF", 
		ReturnURL:     "https://my.website.com/payunit/return", 
		NotifyURL:     &notifyUrl, 
		Name:          &appName, 
		PurchaseRef:   &purchaseRef,
    },
)

if err != nil {
    log.Fatal(err)
}

log.Println(response.Data.TransactionID) // e.g MTNkOWQ0NmE4ZTE2ZGFhMmE4Nzg0ZDNlYWVkODBiOGQwMDc4MzM3MGYzZGM1ZGRj
```

#### Make payment on PayUnit

`POST /gateway/makepayment`: Makepayment makes a payment on Payunit.

**NOTE:** You must initialize a transaction using before calling Makepayment

```go
// Optional request parameters
var notifyUrl = "https://my.website.com/payunit/notify"
var appName = "your app name"

response, _, err := payunitClient.Gateway.Makepayment(
    context.Background(),
    payunit.GatewayMakepaymentRequestPayload{
        Gateway:       "mtnmomo",
        Amount:        100, // Same as the amount you used when initializing the payment
        TransactionID: "your unique transaction id", // Same as the transaction id you used when initializing the payment
        PhoneNumber:   "655487***",
        Currency:      "XAF",
        NotifyURL:     &notifyUrl,
        Name:          &appName,
    },
)
if err != nil {
    log.Fatal(err)
}

log.Println(response.Data["transaction_id"]) // e.g "5222fg525rtrtrt22"
```

## Testing

You can run the unit tests for this SDK from the root directory using the command below:
```bash
go test -v
```

## License

The Payunit golang SDK is released under a proprietary [SevenGPS](https://www.sevengps.net) license.