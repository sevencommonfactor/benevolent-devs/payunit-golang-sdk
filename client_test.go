package payunit

import (
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/url"
	"testing"
)

func TestNew(t *testing.T) {
	t.Run("default configuration is used when no option is set", func(t *testing.T) {
		// act
		client := New()

		// assert
		assert.NotEmpty(t, client.mode)
		assert.NotEmpty(t, client.common)

		assert.Empty(t, client.apiUser)
		assert.Empty(t, client.apiPassword)
		assert.Empty(t, client.apiKey)

		assert.NotNil(t, client.httpClient)
		assert.NotNil(t, client.baseURL)
		assert.NotNil(t, client.Gateway)
	})

	t.Run("single configuration value can be set using options", func(t *testing.T) {
		// Arrange
		newBaseURL, _ := url.Parse("https://example.com")

		// Act
		client := New(WithBaseURL(newBaseURL))

		// Assert
		assert.NotNil(t, client.baseURL)
		assert.Equal(t, newBaseURL.String(), client.baseURL.String())
	})

	t.Run("multiple configuration values can be set using options", func(t *testing.T) {
		// Arrange
		newBaseURL, _ := url.Parse("https://example.com")
		newHTTPClient := &http.Client{Timeout: 422}

		// Act
		client := New(WithBaseURL(newBaseURL), WithHTTPClient(newHTTPClient))

		// Assert
		assert.NotNil(t, client.baseURL)
		assert.Equal(t, newBaseURL.String(), client.baseURL.String())

		assert.NotNil(t, client.httpClient)
		assert.Equal(t, newHTTPClient.Timeout, client.httpClient.Timeout)
	})

	t.Run("it sets the gateway service correctly", func(t *testing.T) {
		// Arrange
		client := New()

		// Assert
		assert.NotNil(t, client.Gateway)
		assert.Equal(t, client.baseURL.String(), client.Gateway.client.baseURL.String())
	})

	t.Run("it sets the gateway service correctly", func(t *testing.T) {
		// Arrange
		client := New()

		// Assert
		assert.NotNil(t, client.Gateway)
		assert.Equal(t, client.baseURL.String(), client.Gateway.client.baseURL.String())
	})
}
