package payunit

type GatewayInitializeResponsePayload struct {
	Status     string `json:"status"`
	StatusCode int    `json:"statusCode"`
	Message    string `json:"message"`
	Data       struct {
		TID            string `json:"t_id"`
		TSum           string `json:"t_sum"`
		TURL           string `json:"t_url"`
		TransactionURL string `json:"transaction_url"`
		TransactionID  string `json:"transaction_id"`
	} `json:"data"`
}
