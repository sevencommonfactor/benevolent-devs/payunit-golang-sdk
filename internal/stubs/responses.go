package stubs

func GatewayMakePaymentResponseMtnPsp() string {
	return `
	{
		"status": "SUCCESS",
		"statusCode": 200,
		"message": "Please confirm the Transaction on your mobile by dialing *126#",
		"data": {
			"payment_ref": "b725d5e6-4612-4e33-89c6-cf3188b57c80",
			"transaction_id": "5222fg525rtrtrt22",
			"pay_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSMjU2In0.eyJjbGllbnRJZCI6ImU3N2E3NmQ5LTg4MTEtNDhlZC******"
		}
	}
`
}

func GatewayMakePaymentResponseOrangePsp() string {
	return `
	{
		"status": "SUCCESS",
		"statusCode": 200,
		"message": "Please confirm the Transaction on your mobile by dialing *126#",
		"data": {
			"auth-token": "b725d5e6-4612-4e33-89c6-cf3188b57c80",
			"transaction_id": "5222fg525rtrtrt22",
			"x-token":"hjhhjhkjkkjhkjh",
			"paytoken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSMjU2In0.eyJjbGllbnRJZCI6ImU3N2E3NmQ5LTg4MTEtNDhlZC******"
		}
	}`
}

func GatewayInitializeResponse() string {
	return `
	{
		"status": "SUCCESS",
		"statusCode": 200,
		"message": "Transaction Created.",
		"data": {
		   "t_id": "MTNkOWQ0NmE4ZTE2ZGFhMmE4Nzg0ZDNlYWVkODBiOGQwMDc4MzM3MGYzZGM1ZGRj",
			"t_sum": "MTYxMjUwMjE4NDBkMjc1YmZhNDVhM2FiNWM4NGVkNTQzNjk5Y2E==",
			"t_url": "ZWNlZGNhY2EzOWFmY2JhYjllMGE3NTVhZTU2ZmYyZjM4MGEyZTliNGI5NWIyMWEy",
			"transaction_url": "https://hostedpages.payunit.net/#/hostedPayment/payment/?t_id=ZDJhZmNhMzJhMWY5NGJlZjhmY2E5MDRlYTUzMjk9370",
			"transaction_id": "5222fg525rtrtrt22"
		}
	}`
}
