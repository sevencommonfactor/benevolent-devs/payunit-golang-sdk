package payunit

type GatewayInitializeRequestPayload struct {
	TotalAmount   uint    `json:"total_amount"`
	TransactionID string  `json:"transaction_id"`
	Currency      string  `json:"currency"`
	ReturnURL     string  `json:"return_url"`
	NotifyURL     *string `json:"notify_url"`
	Name          *string `json:"name"`
	Description   *string `json:"description"`
	PurchaseRef   *string `json:"purchaseRef"`
}
