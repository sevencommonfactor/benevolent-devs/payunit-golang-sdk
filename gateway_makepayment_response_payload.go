package payunit

type GatewayMakepaymentResponsePayload struct {
	Status     string            `json:"status"`
	StatusCode int               `json:"statusCode"`
	Message    string            `json:"message"`
	Data       map[string]string `json:"data"`
}

func (response *GatewayMakepaymentResponsePayload) PaymentRequestID() string {
	if id, ok := response.Data["payment_ref"]; ok {
		return id
	}

	if id, ok := response.Data["auth-token"]; ok {
		return id
	}

	return ""
}
