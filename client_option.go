package payunit

import (
	"net/http"
	"net/url"
)

type ClientOption interface {
	apply(config *clientConfig)
}

type clientOptionFunc func(config *clientConfig)

func (fn clientOptionFunc) apply(config *clientConfig) {
	fn(config)
}

func WithHTTPClient(httpClient *http.Client) ClientOption {
	return clientOptionFunc(func(config *clientConfig) {
		if httpClient != nil {
			config.httpClient = httpClient
		}
	})
}

func WithBaseURL(baseURL *url.URL) ClientOption {
	return clientOptionFunc(func(config *clientConfig) {
		if baseURL != nil {
			config.baseURL = baseURL
		}
	})
}

func WithAPIUser(apiUser string) ClientOption {
	return clientOptionFunc(func(config *clientConfig) {
		config.apiUser = apiUser
	})
}

func WithAPIPassword(apiPassword string) ClientOption {
	return clientOptionFunc(func(config *clientConfig) {
		config.apiPassword = apiPassword
	})
}

func WithAPIKey(apiKey string) ClientOption {
	return clientOptionFunc(func(config *clientConfig) {
		config.apiKey = apiKey
	})
}

func WithMode(mode string) ClientOption {
	return clientOptionFunc(func(config *clientConfig) {
		config.mode = mode
	})
}
