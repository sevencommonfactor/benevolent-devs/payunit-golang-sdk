package payunit

// GatewayMakePaymentCallbackRequestPayload is the payload of the callback request sent to your `notify_url`
// of the `makePayment` request.
type GatewayMakePaymentCallbackRequestPayload struct {
	TransactionID     string `json:"transaction_id"`
	TransactionAmount string `json:"transaction_amount"`
	TransactionStatus string `json:"transaction_status"`
	Message           string `json:"message"`
	Error             string `json:"error"`
}

// IsSuccessfull returns true if the transaction is successfull and false otherwise
func (payload GatewayMakePaymentCallbackRequestPayload) IsSuccessfull() bool {
	return payload.TransactionStatus == "SUCCESS"
}

// IsFailed returns true if the transaction failed
func (payload GatewayMakePaymentCallbackRequestPayload) IsFailed() bool {
	return payload.TransactionStatus == "FAILED"
}
