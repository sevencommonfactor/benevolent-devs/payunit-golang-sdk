package payunit

import (
	"net/http"
	"net/url"
)

const (
	apiBaseURL = "https://app.payunit.net/api/"
)

type clientConfig struct {
	httpClient  *http.Client
	baseURL     *url.URL
	apiKey      string
	apiPassword string
	apiUser     string
	mode        string
}

func defaultClientConfig() *clientConfig {
	endpoint, _ := url.Parse(apiBaseURL)

	return &clientConfig{
		httpClient:  http.DefaultClient,
		baseURL:     endpoint,
		apiKey:      "",
		apiPassword: "",
		apiUser:     "",
		mode:        "live",
	}
}
