package payunit

import (
	"context"
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"gitlab.com/sevencommonfactor/benevolent-devs/payunit-golang-sdk/internal/stubs"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"
	"time"
)

//nolint:funlen
func TestGatewayService_MakePayment(t *testing.T) {
	t.Run("it returns a 400 error when the api credentials are invalid", func(t *testing.T) {
		// Arrange
		gatewayService := &GatewayService{client: New()}

		// Act
		_, response, err := gatewayService.Makepayment(context.Background(), GatewayMakepaymentRequestPayload{})

		// Assert
		assert.NotNil(t, err)
		assert.Equal(t, http.StatusBadRequest, response.HTTPResponse.StatusCode)
	})

	t.Run("it returns an error when the response code is not 200", func(t *testing.T) {
		// Arrange
		body   := "invalid transaction id"
		server := makeTestServer(http.StatusBadRequest, body)
		baseURL, _ := url.Parse(server.URL)
		gatewayService := &GatewayService{client: New(WithBaseURL(baseURL))}
		defer server.Close()

		// Act
		_, response, err := gatewayService.Makepayment(context.Background(), GatewayMakepaymentRequestPayload{})

		// Assert
		assert.NotNil(t, err)
		assert.Contains(t, err.Error(), body)
		assert.Equal(t, http.StatusBadRequest, response.HTTPResponse.StatusCode)
	})

	t.Run("it returns an error when the context is expired", func(t *testing.T) {
		// Arrange
		ctx, cancel := context.WithDeadline(context.Background(), time.Now())
		defer cancel()
		gatewayService := &GatewayService{client: New()}

		// Act
		_, _, err := gatewayService.Makepayment(ctx, GatewayMakepaymentRequestPayload{})

		// Assert
		assert.NotNil(t, err)
		assert.True(t, strings.Contains(err.Error(), "context deadline exceeded"))
	})

	t.Run("it returns a valid response mtn psp response", func(t *testing.T) {
		// Arrange
		server := makeTestServer(http.StatusOK, stubs.GatewayMakePaymentResponseMtnPsp())
		baseURL, _ := url.Parse(server.URL)
		gatewayService := &GatewayService{
			client: New(WithBaseURL(baseURL)),
		}
		defer server.Close()

		// Act
		payload, response, err := gatewayService.Makepayment(context.Background(), GatewayMakepaymentRequestPayload{})

		// Assert
		assert.NoError(t, err)

		assert.Equal(t, http.StatusOK, response.HTTPResponse.StatusCode)

		assert.Equal(t, "SUCCESS", payload.Status)
		assert.Equal(t, 200, payload.StatusCode)
		assert.Equal(t, "Please confirm the Transaction on your mobile by dialing *126#", payload.Message)
		assert.Equal(t, "b725d5e6-4612-4e33-89c6-cf3188b57c80", payload.Data["payment_ref"])
		assert.Equal(t, "5222fg525rtrtrt22", payload.Data["transaction_id"])
		assert.Equal(t, "eyJ0eXAiOiJKV1QiLCJhbGciOiJSMjU2In0.eyJjbGllbnRJZCI6ImU3N2E3NmQ5LTg4MTEtNDhlZC******", payload.Data["pay_token"])
	})

	t.Run("it returns a valid response orange psp response", func(t *testing.T) {
		// Arrange
		server := makeTestServer(http.StatusOK, stubs.GatewayMakePaymentResponseOrangePsp())
		baseURL, _ := url.Parse(server.URL)
		gatewayService := &GatewayService{
			client: New(WithBaseURL(baseURL)),
		}
		defer server.Close()

		// Act
		payload, response, err := gatewayService.Makepayment(context.Background(), GatewayMakepaymentRequestPayload{})

		// Assert
		assert.NoError(t, err)

		assert.Equal(t, http.StatusOK, response.HTTPResponse.StatusCode)

		assert.Equal(t, "SUCCESS", payload.Status)
		assert.Equal(t, 200, payload.StatusCode)
		assert.Equal(t, "Please confirm the Transaction on your mobile by dialing *126#", payload.Message)
		assert.Equal(t, "b725d5e6-4612-4e33-89c6-cf3188b57c80", payload.Data["auth-token"])
		assert.Equal(t, "5222fg525rtrtrt22", payload.Data["transaction_id"])
		assert.Equal(t, "hjhhjhkjkkjhkjh", payload.Data["x-token"])
		assert.Equal(t, "eyJ0eXAiOiJKV1QiLCJhbGciOiJSMjU2In0.eyJjbGllbnRJZCI6ImU3N2E3NmQ5LTg4MTEtNDhlZC******", payload.Data["paytoken"])
	})

	t.Run("it returns an error when the base URL is invalid", func(t *testing.T) {
		// Arrange
		server := makeTestServer(http.StatusOK, stubs.GatewayMakePaymentResponseOrangePsp())
		baseURL, _ := url.Parse("sftp://example.com")
		gatewayService := &GatewayService{
			client: New(WithBaseURL(baseURL)),
		}
		defer server.Close()

		// Act
		_, _, err := gatewayService.Makepayment(context.Background(), GatewayMakepaymentRequestPayload{})

		// Assert
		assert.NotNil(t, err)
	})

	t.Run("it returns an error when the payload is invalid", func(t *testing.T) {
		// Arrange
		server := makeTestServer(http.StatusOK, `{322}`)
		baseURL, _ := url.Parse(server.URL)
		gatewayService := &GatewayService{client: New(WithBaseURL(baseURL))}
		defer server.Close()

		// Act
		_, response, err := gatewayService.Makepayment(context.Background(), GatewayMakepaymentRequestPayload{})

		// Assert
		assert.NotNil(t, err)

		assert.Equal(t, http.StatusOK, response.HTTPResponse.StatusCode)

		assert.Equal(t, []byte("{322}"), *response.Body)
	})

	t.Run("it sends the correct payload to the server", func(t *testing.T) {
		// Arrange
		var serverRequest map[string]interface{}
		server := httptest.NewServer(http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
			data, err := ioutil.ReadAll(req.Body)
			if err != nil {
				panic(err)
			}

			err = json.Unmarshal(data, &serverRequest)
			if err != nil {
				panic(err)
			}

			res.WriteHeader(http.StatusOK)
			_, err = res.Write([]byte(stubs.GatewayMakePaymentResponseMtnPsp()))
			if err != nil {
				panic(err)
			}
		}))

		baseURL, _ := url.Parse(server.URL)
		gatewayService := &GatewayService{client: New(WithBaseURL(baseURL))}
		defer server.Close()

		strPtr := func(val string) *string { return &val }

		payload := GatewayMakepaymentRequestPayload{
			Gateway:       "mtnmomo",
			Amount:        1000,
			TransactionID: "abc-xyz",
			PhoneNumber:   "677777777",
			Currency:      "XAF",
			NotifyURL:     strPtr("https://example.com"),
			Name:          strPtr("TestName"),
		}

		// Act
		_, _, err := gatewayService.Makepayment(context.Background(), payload)

		// Assert
		assert.Nil(t, err)

		assert.Equal(t, payload.Gateway, serverRequest["gateway"].(string))
		assert.Equal(t, payload.Amount, uint(serverRequest["amount"].(float64)))
		assert.Equal(t, payload.TransactionID, serverRequest["transaction_id"].(string))
		assert.Equal(t, payload.PhoneNumber, serverRequest["phone_number"].(string))
		assert.Equal(t, payload.Currency, serverRequest["currency"].(string))
		assert.Equal(t, *payload.NotifyURL, serverRequest["notify_url"].(string))
		assert.Equal(t, *payload.Name, serverRequest["name"].(string))
	})
}

func TestGatewayService_Initialize(t *testing.T) {
	t.Run("it returns a 400 error when the api credentials are invalid", func(t *testing.T) {
		// Arrange
		gatewayService := &GatewayService{client: New()}

		// Act
		_, response, err := gatewayService.Initialize(context.Background(), GatewayInitializeRequestPayload{})

		// Assert
		assert.NotNil(t, err)
		assert.Equal(t, http.StatusBadRequest, response.HTTPResponse.StatusCode)
	})

	t.Run("it returns an error when the response code is not 200", func(t *testing.T) {
		// Arrange
		body   := "invalid transaction id"
		server := makeTestServer(http.StatusBadRequest, body)
		baseURL, _ := url.Parse(server.URL)
		gatewayService := &GatewayService{client: New(WithBaseURL(baseURL))}
		defer server.Close()

		// Act
		_, response, err := gatewayService.Initialize(context.Background(), GatewayInitializeRequestPayload{})

		// Assert
		assert.NotNil(t, err)
		assert.Contains(t, err.Error(), body)
		assert.Equal(t, http.StatusBadRequest, response.HTTPResponse.StatusCode)
	})

	t.Run("it returns an error when the context is expired", func(t *testing.T) {
		// Arrange
		ctx, cancel := context.WithDeadline(context.Background(), time.Now())
		defer cancel()
		gatewayService := &GatewayService{client: New()}

		// Act
		_, _, err := gatewayService.Initialize(ctx, GatewayInitializeRequestPayload{})

		// Assert
		assert.NotNil(t, err)
		assert.True(t, strings.Contains(err.Error(), "context deadline exceeded"))
	})

	t.Run("it returns a valid response from the payload", func(t *testing.T) {
		// Arrange
		server := makeTestServer(http.StatusOK, stubs.GatewayInitializeResponse())
		baseURL, _ := url.Parse(server.URL)
		gatewayService := &GatewayService{
			client: New(WithBaseURL(baseURL)),
		}
		defer server.Close()

		// Act
		payload, response, err := gatewayService.Initialize(context.Background(), GatewayInitializeRequestPayload{})

		// Assert
		assert.NoError(t, err)

		assert.Equal(t, http.StatusOK, response.HTTPResponse.StatusCode)

		assert.Equal(t, "SUCCESS", payload.Status)
		assert.Equal(t, 200, payload.StatusCode)
		assert.Equal(t, "Transaction Created.", payload.Message)
		assert.Equal(t, "MTNkOWQ0NmE4ZTE2ZGFhMmE4Nzg0ZDNlYWVkODBiOGQwMDc4MzM3MGYzZGM1ZGRj", payload.Data.TID)
		assert.Equal(t, "MTYxMjUwMjE4NDBkMjc1YmZhNDVhM2FiNWM4NGVkNTQzNjk5Y2E==", payload.Data.TSum)
		assert.Equal(t, "ZWNlZGNhY2EzOWFmY2JhYjllMGE3NTVhZTU2ZmYyZjM4MGEyZTliNGI5NWIyMWEy", payload.Data.TURL)
		assert.Equal(t, "https://hostedpages.payunit.net/#/hostedPayment/payment/?t_id=ZDJhZmNhMzJhMWY5NGJlZjhmY2E5MDRlYTUzMjk9370", payload.Data.TransactionURL)
		assert.Equal(t, "5222fg525rtrtrt22", payload.Data.TransactionID)
	})

	t.Run("it returns an error when the base URL is invalid", func(t *testing.T) {
		// Arrange
		server := makeTestServer(http.StatusOK, stubs.GatewayMakePaymentResponseOrangePsp())
		baseURL, _ := url.Parse("sftp://example.com")
		gatewayService := &GatewayService{
			client: New(WithBaseURL(baseURL)),
		}
		defer server.Close()

		// Act
		_, _, err := gatewayService.Initialize(context.Background(), GatewayInitializeRequestPayload{})

		// Assert
		assert.NotNil(t, err)
	})

	t.Run("it returns an error when the payload is invalid", func(t *testing.T) {
		// Arrange
		server := makeTestServer(http.StatusOK, `{322}`)
		baseURL, _ := url.Parse(server.URL)
		gatewayService := &GatewayService{client: New(WithBaseURL(baseURL))}
		defer server.Close()

		// Act
		_, response, err := gatewayService.Initialize(context.Background(), GatewayInitializeRequestPayload{})

		// Assert
		assert.NotNil(t, err)

		assert.Equal(t, http.StatusOK, response.HTTPResponse.StatusCode)

		assert.Equal(t, []byte("{322}"), *response.Body)
	})

	t.Run("it sends the correct payload to the server", func(t *testing.T) {
		// Arrange
		var serverRequest map[string]interface{}
		server := httptest.NewServer(http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
			data, err := ioutil.ReadAll(req.Body)
			if err != nil {
				panic(err)
			}

			err = json.Unmarshal(data, &serverRequest)
			if err != nil {
				panic(err)
			}

			res.WriteHeader(http.StatusOK)
			_, err = res.Write([]byte(stubs.GatewayInitializeResponse()))
			if err != nil {
				panic(err)
			}
		}))

		baseURL, _ := url.Parse(server.URL)
		gatewayService := &GatewayService{client: New(WithBaseURL(baseURL))}
		defer server.Close()

		strPtr := func(val string) *string { return &val }

		payload := GatewayInitializeRequestPayload{
			TotalAmount:   200,
			TransactionID: "abc-xyz",
			Currency:      "XAF",
			ReturnURL:     "",
			NotifyURL:     strPtr("https://example.com"),
			Name:          strPtr("TestName"),
			Description:   strPtr("TestDescription"),
			PurchaseRef:   strPtr("TestPurchaseRef"),
		}

		// Act
		_, _, err := gatewayService.Initialize(context.Background(), payload)

		// Assert
		assert.Nil(t, err)

		assert.Equal(t, payload.TotalAmount, uint(serverRequest["total_amount"].(float64)))
		assert.Equal(t, payload.TransactionID, serverRequest["transaction_id"].(string))
		assert.Equal(t, payload.Currency, serverRequest["currency"].(string))
		assert.Equal(t, payload.ReturnURL, serverRequest["return_url"].(string))
		assert.Equal(t, *payload.NotifyURL, serverRequest["notify_url"].(string))
		assert.Equal(t, *payload.Name, serverRequest["name"].(string))
		assert.Equal(t, *payload.Description, serverRequest["description"].(string))
		assert.Equal(t, *payload.PurchaseRef, serverRequest["purchaseRef"].(string))
	})
}

func makeTestServer(responseCode int, body string) *httptest.Server {
	return httptest.NewServer(http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		res.WriteHeader(responseCode)
		_, err := res.Write([]byte(body))
		if err != nil {
			panic(err)
		}
	}))
}
