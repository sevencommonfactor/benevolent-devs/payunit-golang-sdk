package payunit

import (
	"bytes"
	"errors"
	"net/http"
	"strconv"
)

type Response struct {
	HTTPResponse *http.Response
	Body         *[]byte
}

func (r *Response) Error() error {
	switch r.HTTPResponse.StatusCode {
	case 200, 201, 202, 204, 205:
		return nil
	default:
		return errors.New(r.errorMessage())
	}
}
func (r *Response) errorMessage() string {
	var buf bytes.Buffer
	buf.WriteString(strconv.Itoa(r.HTTPResponse.StatusCode))
	buf.WriteString(": ")
	buf.WriteString(http.StatusText(r.HTTPResponse.StatusCode))
	buf.WriteString(", Body: ")
	buf.Write(*r.Body)

	return buf.String()
}
