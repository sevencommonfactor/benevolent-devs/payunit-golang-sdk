package payunit

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestGatewayMakePaymentResponsePayload_PaymentRequestID(t *testing.T) {
	t.Run("it returns the Data[payment_ref] if it is set", func(t *testing.T) {
		// Arrange
		expectedID := "b725d5e6-4612-4e33-89c6-cf3188b57c80"
		response := GatewayMakepaymentResponsePayload{
			Data: map[string]string{
				"payment_ref": expectedID,
			},
		}

		// Act
		actualID := response.PaymentRequestID()

		// Assert
		assert.Equal(t, expectedID, actualID)
	})

	t.Run("it returns the Data[auth-token] if it is set", func(t *testing.T) {
		// Arrange
		expectedID := "b725d5e6-4612-4e33-89c6-cf3188b57c80"
		response := GatewayMakepaymentResponsePayload{
			Data: map[string]string{
				"auth-token": expectedID,
			},
		}

		// Act
		actualID := response.PaymentRequestID()

		// Assert
		assert.Equal(t, expectedID, actualID)
	})

	t.Run("it returns the empty string if no payment id is available", func(t *testing.T) {
		// Arrange
		expectedID := ""
		response := GatewayMakepaymentResponsePayload{}

		// Act
		actualID := response.PaymentRequestID()

		// Assert
		assert.Equal(t, expectedID, actualID)
	})
}
