package payunit

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
)

type service struct {
	client *Client
}

// Client is the payunit API client.
// Do not instantiate this client with Client{}. Use the New method instead.
type Client struct {
	httpClient  *http.Client
	common      service
	baseURL     *url.URL
	apiKey      string
	apiUser     string
	apiPassword string
	mode        string
	Gateway     *GatewayService
}

// New creates and returns a new payunit.Client from a slice of payunit.ClientOption.
func New(options ...ClientOption) *Client {
	config := defaultClientConfig()

	for _, option := range options {
		option.apply(config)
	}

	client := &Client{
		httpClient:  config.httpClient,
		baseURL:     config.baseURL,
		apiKey:      config.apiKey,
		apiUser:     config.apiUser,
		mode: config.mode,
		apiPassword: config.apiPassword,
	}

	client.common.client = client
	client.Gateway = (*GatewayService)(&client.common)
	return client
}

// newRequest creates an API request. A relative URL can be provided in uri,
// in which case it is resolved relative to the BaseURL of the Client.
// URI's should always be specified without a preceding slash.
func (c *Client) newRequest(ctx context.Context, method, uri string, body interface{}) (*http.Request, error) {
	if !strings.HasSuffix(c.baseURL.Path, "/") && !strings.HasPrefix(uri, "/") {
		uri = "/" + uri
	}

	u, err := c.baseURL.Parse(uri)
	if err != nil {
		return nil, err
	}

	var buf io.ReadWriter
	if body != nil {
		buf = &bytes.Buffer{}
		enc := json.NewEncoder(buf)
		enc.SetEscapeHTML(false)
		err := enc.Encode(body)
		if err != nil {
			return nil, err
		}
	}

	req, err := http.NewRequestWithContext(ctx, method, u.String(), buf)
	if err != nil {
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("x-api-key", c.apiKey)
	req.Header.Set("mode", c.mode)

	req.SetBasicAuth(c.apiUser, c.apiPassword)

	return req, nil
}

// do carries out an HTTP request and returns a Response
func (c *Client) do(req *http.Request) (*Response, error) {
	if req == nil {
		return nil, fmt.Errorf("%T cannot be nil", req)
	}

	httpResponse, err := c.httpClient.Do(req)
	if err != nil {
		return nil, err
	}

	defer func() { _ = httpResponse.Body.Close() }()

	resp, err := c.newResponse(httpResponse)
	if err != nil {
		return resp, err
	}

	_, err = io.Copy(ioutil.Discard, httpResponse.Body)
	if err != nil {
		return resp, err
	}

	return resp, nil
}

// newResponse converts an *http.Response to *Response
func (c *Client) newResponse(httpResponse *http.Response) (*Response, error) {
	if httpResponse == nil {
		return nil, fmt.Errorf("%T cannot be nil", httpResponse)
	}

	resp := new(Response)
	resp.HTTPResponse = httpResponse

	buf, err := ioutil.ReadAll(resp.HTTPResponse.Body)
	if err != nil {
		return nil, err
	}
	resp.Body = &buf

	return resp, resp.Error()
}
